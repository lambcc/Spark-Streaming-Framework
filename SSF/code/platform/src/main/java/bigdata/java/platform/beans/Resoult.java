package bigdata.java.platform.beans;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.HashMap;
import java.util.Map;

public class Resoult {
    private int code;
    private String msg;

    private Map<String, Object> data = new HashMap<String, Object>();

    public Resoult add(String key,Object value){
        this.getData().put(key, value);
        return this;
    }


    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }
    public Resoult setCode(int code) {
        this.code = code;
        return this;
    }
    public String getMsg() {
        return msg;
    }
    public Resoult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    @Override
    public String toString() {
        String json = JSON.toJSONString(this, SerializerFeature.DisableCircularReferenceDetect,SerializerFeature.WriteDateUseDateFormat);
        return json;
    }

    public static Resoult success(){
        Resoult result = new Resoult();
        result.setCode(0);
        result.setMsg("处理成功！");
        return result;
    }

    public static Resoult fail(){
        Resoult result = new Resoult();
        result.setCode(1);
        result.setMsg("处理失败！");
        return result;
    }
}
