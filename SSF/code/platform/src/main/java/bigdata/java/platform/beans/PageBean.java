package bigdata.java.platform.beans;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class PageBean <T>{

    private List<T> pageData;
    private Integer currentPage = Integer.valueOf(1);
    private Integer pageSize = Integer.valueOf(10);
    private Integer totalCount;

    public int getPageCount(){
        if (this.totalCount.intValue() % this.pageSize.intValue() == 0) {
            return this.totalCount.intValue() / this.pageSize.intValue();
        }
        return this.totalCount.intValue() / this.pageSize.intValue() + 1;
    }
    public PageBean(List<T> pageData, Integer totalCount) {
        this.pageData = pageData;
        this.totalCount = totalCount;
    }

    public PageBean(List<T> pageData, Integer totalCount,Integer currentPage) {
        this.pageData = pageData;
        this.totalCount = totalCount;
        setCurrentPage(currentPage);
    }

    public PageBean() {}

    public boolean isFirst()
    {
        return (this.currentPage.intValue() == 1) || (this.totalCount.intValue() == 0);
    }

    public boolean isLast() {
        return (this.totalCount.intValue() == 0) || (this.currentPage.intValue() >= getPageCount());
    }

    public List<T> getRAMPageData()
    {
        if(null==this.pageData || this.pageData.size() == 0)
            return new ArrayList<T>();
        int totalRowNum = this.pageData.size();
        int totalPageNum = (totalRowNum - 1) / pageSize + 1;

        int realPageNo = currentPage;
        if (currentPage > totalPageNum) {
            realPageNo = totalPageNum;
        } else if (currentPage < 1) {
            realPageNo = 1;
        }

        int fromIdx = (realPageNo - 1) * pageSize;
        int toIdx = realPageNo * pageSize;

        if (realPageNo == totalPageNum && totalPageNum * pageSize >= totalRowNum) {
            toIdx = totalRowNum;
        }
        List<T> result = this.pageData.subList(fromIdx, toIdx);
        return result;
    }

    public boolean isHasNext()
    {
        return this.currentPage.intValue() < getPageCount();
    }

    public boolean isHasPrev() {
        return this.currentPage.intValue() > 1;
    }
    public Integer getNextPage()
    {
        if (this.currentPage.intValue() >= getPageCount()) {
            return Integer.valueOf(getPageCount());
        }
        return Integer.valueOf(this.currentPage.intValue() + 1);
    }

    public Integer getPrevPage() {
        if (this.currentPage.intValue() <= 1) {
            return Integer.valueOf(1);
        }
        return Integer.valueOf(this.currentPage.intValue() - 1);
    }

    public List<T> getPageData() {
        return this.pageData;
    }

    public void setPageData(List<T> pageData) {
        this.pageData = pageData;
    }

    public Integer getCurrentPage() {
        if(this.getPageCount()==0)
            return 0;
        return this.currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}

