package bigdata.java.platform.util;

import bigdata.java.platform.beans.SparkJob;
import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.beans.TTask;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class LivyUtil {

    static LivyUtil livyUtil;
    @Autowired
    SysSet sysSet;

    public static SysSet getSysSet()
    {
        return livyUtil.sysSet;
    }

    @PostConstruct
    public void init()
    {
        livyUtil = this;
        livyUtil.sysSet = this.sysSet;
    }

    @Autowired
    public void setSysSet(SysSet sysSet)
    {
        this.sysSet = sysSet;
    }

    public static String getJars(TTask tTask)
    {
        String sparkApplication = livyUtil.sysSet.getSparkApplication();
//        String jars = Comm.taskFiles(sparkApplication, "jars", tTask.getJars());
        String jars = Comm.taskFiles(sparkApplication, "jars", tTask);
        if(!StringUtils.isBlank(jars))
        {
            jars = "\"jars\":[" + jars +"]";
        }
        return jars;
    }

    public static String getFiles(TTask tTask)
    {
        String sparkApplication = livyUtil.sysSet.getSparkApplication();
//        String files = Comm.taskFiles(sparkApplication, "files", tTask.getFiles());
        String files = Comm.taskFiles(sparkApplication, "files", tTask);
        if(!StringUtils.isBlank(files))
        {
            files = "\"files\":[" + files +"]";
        }
        return files;
    }

    public static String getConf(TTask tTask)
    {
        String conf= "";
        if(!StringUtils.isBlank(tTask.getConf()))
        {
            conf = "\"conf\":{" + tTask.getConf() + "}";
        }

        return conf;
    }

    public static String getParam(TTask tTask)
    {
        String sparkApplication = livyUtil.sysSet.getSparkApplication();
        SparkJob job = tTask.getJob();
//        String proxyUser= ConfigUtil.getConfig().getString("SysSet.hdfsAccount");
        String proxyUser= tTask.getQueueName();
        String param = "\"file\":\"" + sparkApplication + job.getHdfsPath() + "\",\"className\":\""+tTask.getClassFullName()+"\",\"name\":\""+tTask.getAppName() + "\"," + tTask.getParam()+",\"queue\":\""+tTask.getQueueName()+"\""+ ",\"proxyUser\":\""+proxyUser+"\"";
        return param;
    }

    public static String getArgs(TTask tTask)
    {
        String args="";
        if(tTask.getTaskType().intValue()==1)
        {//内部
            args = "\"args\":[\"appname:"+tTask.getAppName()+"\","+tTask.getArgs() + ",\"groupid:"+tTask.getAppName()+"\"]";
        }
        if(tTask.getTaskType().intValue()==10)
        {//外部
            String[] splits = tTask.getArgs().trim().split(" ");
            for (int i = 0; i < splits.length; i++) {
                splits[i] = "\""+splits[i]+"\"";
            }
            args = "\"args\":["+StringUtils.join(splits,",")+ "]";
        }
        return args;
    }

    public static String getLivyCURL(TTask tTask)
    {
        String curl = "curl -X POST -H \"Content-Type: application/json\" -H \"X-Requested-By:user\" "+livyUtil.sysSet.getLivy()+"/batches --data '{";
        curl+= getConf(tTask) + "," + getParam(tTask) + "," + getFiles(tTask) + "," + getArgs(tTask) + "," + getJars(tTask) +"}'";
        return curl;
    }
    public static String getJson(TTask tTask)
    {
//        String json ="{" + getConf(tTask) + "," + getParam(tTask) + "," + getFiles(tTask) + "," + getArgs(tTask) + "," + getJars(tTask) +"}";
        String json = "{";
        if(!StringUtils.isBlank(tTask.getConf()))
        {
            json += getConf(tTask) + ",";
        }

        if(!StringUtils.isBlank(tTask.getParam()))
        {
            json += getParam(tTask) + ",";
        }
        else
        {
            SparkJob job = tTask.getJob();
//            String proxyUser= ConfigUtil.getConfig().getString("SysSet.hdfsAccount");
            String proxyUser= tTask.getQueueName();
            json += "\"file\":\""+job.getHdfsPath()+"\",\"className\":\""+tTask.getClassFullName()+"\",\"name\":\""+tTask.getAppName() + "\",\"queue\":\""+tTask.getQueueName()+"\","+ "\"proxyUser\":\""+ proxyUser +"\",";
        }

        if(!StringUtils.isBlank(tTask.getFiles()))
        {
            json += getFiles(tTask) + ",";
        }

        if(!StringUtils.isBlank(tTask.getArgs()))
        {
            json += getArgs(tTask) + ",";
        }

        if(!StringUtils.isBlank(tTask.getJars()))
        {
            json += getJars(tTask) + ",";
        }

        json =json.substring(0,json.length() -1 )+"}";

        return json;
    }

    public static String[] getLivyCMDS(TTask tTask)
    {
        String[] cmds = new String[]{"curl", "-X", "POST", "-H", "\"Content-Type: application/json\"", "-H", "\"X-Requested-By:user\"",livyUtil.sysSet.getLivy()+"/batches", "--data", "'{" + getConf(tTask) + "," + getParam(tTask) + "," + getFiles(tTask) + "," + getArgs(tTask) + "," + getJars(tTask) +"}'"};
        return cmds;
    }
}
