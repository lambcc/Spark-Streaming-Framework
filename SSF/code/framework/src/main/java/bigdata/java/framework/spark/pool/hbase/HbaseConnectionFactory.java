/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.hbase;

import bigdata.java.framework.spark.pool.ConnectionFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HbaseConnectionFactory implements ConnectionFactory<Connection> {

    HbaseConfig hbaseConfig;
    public HbaseConnectionFactory(HbaseConfig hbaseConfig)
    {
        this.hbaseConfig = hbaseConfig;
    }

    @Override
    public Connection createConnection() throws Exception {
        Properties properties = hbaseConfig.getProperties();
        Configuration hadoopConfiguration = HBaseConfiguration.create();

        Set<Map.Entry<Object, Object>> entries = properties.entrySet();
        for (Map.Entry<Object,Object> entry : entries)
        {
            String key = entry.getKey().toString();
            String value = entry.getValue().toString();
            hadoopConfiguration.set(key,value);
        }
        int maxTotal = hbaseConfig.getMaxTotal();
        ExecutorService executor = Executors.newFixedThreadPool(maxTotal);
//        ExecutorService executor = Executors.newFixedThreadPool(hbaseConfig.getPoolSize());
        Connection connection = org.apache.hadoop.hbase.client.ConnectionFactory.createConnection(hadoopConfiguration,executor);
        return connection;
    }

    @Override
    public PooledObject<Connection> makeObject() throws Exception {
        Connection connection = this.createConnection();
        return new DefaultPooledObject<Connection>(connection);
    }

    @Override
    public void destroyObject(PooledObject<Connection> pooledObject) throws Exception {
        Connection connection = pooledObject.getObject();
        if (connection != null)
        {
            connection.close();
        }
    }

    @Override
    public boolean validateObject(PooledObject<Connection> pooledObject) {
        Connection connection = pooledObject.getObject();
        if (connection != null)
        {
            return ((!connection.isAborted()) && (!connection.isClosed()));
        }
        return false;
    }

    @Override
    public void activateObject(PooledObject<Connection> pooledObject) throws Exception {

    }

    @Override
    public void passivateObject(PooledObject<Connection> pooledObject) throws Exception {

    }
}
