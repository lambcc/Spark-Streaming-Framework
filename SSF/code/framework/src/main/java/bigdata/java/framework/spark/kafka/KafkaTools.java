/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.ConfigUtil;
import bigdata.java.framework.spark.util.DateUtil;
import bigdata.java.framework.spark.util.InputParameterUtil;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import kafka.api.PartitionOffsetRequestInfo;
import kafka.cluster.BrokerEndPoint;
import kafka.common.TopicAndPartition;
import kafka.javaapi.*;
import kafka.javaapi.consumer.SimpleConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.*;
import scala.Tuple2;

import java.util.*;
import java.util.stream.Stream;

/**
 * kafka 工具类
 */
public class KafkaTools  {

    public static final String GROUPID = "group.id";

    public static Map<String, Object> createGeneralParams()
    {
        return createGeneralParams("application.properties");
    }

    public static Map<String, Object> createGeneralParams(String properties)
    {
        Config config = ConfigFactory.load(properties);
        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        Stream<Map.Entry<String, ConfigValue>> filterConfig = config.entrySet().stream()
                .filter(e -> e.getKey().startsWith("kafkaparam."));
        filterConfig.forEach(e->kafkaParams.put(e.getKey().replace("kafkaparam.","")
                ,config.getString(e.getKey())));
        return kafkaParams;
    }

    public static void printKafkaOffset(JavaInputDStream<ConsumerRecord<String, String>> directStream)
    {
        directStream.foreachRDD(new VoidFunction<JavaRDD<ConsumerRecord<String, String>>>() {
            @Override
            public void call(JavaRDD<ConsumerRecord<String, String>> rdd) throws Exception {
                printKafkaOffset(rdd);
            }
        });
    }

    public static void printKafkaOffset(JavaRDD<ConsumerRecord<String, String>> rdd)
    {
        OffsetRange[] offsetRanges = ((HasOffsetRanges) rdd.rdd()).offsetRanges();
        ArrayList<OffsetRange> rangeArrayList = new ArrayList<>(Arrays.asList(offsetRanges));
        //排序
        rangeArrayList.sort(Comparator.comparing(OffsetRange::topic).thenComparing(OffsetRange::partition));
        for (int i = 0; i < rangeArrayList.size(); i++) {
            OffsetRange o = rangeArrayList.get(i);
            System.out.println(
                    "topic：" + o.topic() + "，partition：" + o.partition() + "，fromOffset：" + o.fromOffset() + "， untilOffset：" + o.untilOffset()+"，size：" + ( o.untilOffset() - o.fromOffset() ));
//                    o.topic() + "," + o.partition() + "," + o.fromOffset() + "," + o.untilOffset()+"," + ( o.untilOffset() - o.fromOffset() ));
        }

//        for (int i = 0; i < offsetRanges.length; i++) {
//            OffsetRange o = offsetRanges[i];
//            System.out.println(
//                    "topic:" + o.topic() + ", partition:" + o.partition() + ", fromOffset:" + o.fromOffset() + ", untilOffset:" + o.untilOffset());
//        }

//        rdd.foreachPartition(consumerRecords -> {
//            OffsetRange o = offsetRanges[TaskContext.get().partitionId()];
//            System.out.println(
//                    "topic:" + o.topic() + ", partition:" + o.partition() + ", fromOffset:" + o.fromOffset() + ", untilOffset:" + o.untilOffset());
//        });
        System.out.println("--------------------------------------------------------------");
    }

    public static void saveKafkaOffset(
            JavaInputDStream<ConsumerRecord<String, String>> directStream
            ,KafkaOffsetPersist kafkaOffsetPersist)
    {
        directStream.foreachRDD((VoidFunction<JavaRDD<ConsumerRecord<String, String>>>) rdd ->
                saveKafkaOffset(rdd,kafkaOffsetPersist)
        );
    }

    public static void commitAsync(JavaInputDStream<ConsumerRecord<String, String>> directStream)
    {
        directStream.foreachRDD((VoidFunction<JavaRDD<ConsumerRecord<String, String>>>) rdd -> {
            OffsetRange[] offsetRanges = ((HasOffsetRanges) rdd.rdd()).offsetRanges();
            CanCommitOffsets canCommitOffsets = (CanCommitOffsets)directStream.inputDStream();
            canCommitOffsets.commitAsync(offsetRanges);
        });
    }

    public static KafkaOffsetPersist getKafkaOffsetPersist(InputParameterUtil inputParameterUtil )
    {
        return getKafkaOffsetPersist(inputParameterUtil.getAppName(),inputParameterUtil.getTopics(),inputParameterUtil.getGroupId());
    }

    public static KafkaOffsetPersist getKafkaOffsetPersist(String appName,String topics,String groupId)
    {
        Config config = ConfigUtil.getConfig();
        if(!config.getIsNull("KafkaOffsetPersist"))
        {
            String kafkaOffsetPersist = config.getString("KafkaOffsetPersist");
            if("MySql".equals(kafkaOffsetPersist))
            {
                return new KafkaOffsetPersistForMySql(appName,topics,groupId);
            }
            else if("Hbase".equals(kafkaOffsetPersist))
            {
                return new KafkaOffsetPersistForHbase(appName,topics,groupId);
            }
            else if("Redis".equals(kafkaOffsetPersist))
            {
                return new KafkaOffsetPersistForRedis(appName,topics,groupId);
            }
            else
            {
                return new KafkaOffsetPersistForHbase(appName,topics,groupId);
            }
        }

        return new KafkaOffsetPersistForHbase(appName,topics,groupId);
    }


    public static void saveKafkaOffset(
            JavaRDD<ConsumerRecord<String, String>> rdd
            ,KafkaOffsetPersist kafkaOffsetPersist)
    {
        OffsetRange[] offsetRanges = ((HasOffsetRanges) rdd.rdd()).offsetRanges();

       //printKafkaOffset(rdd);
        kafkaOffsetPersist.persistOffset(offsetRanges);
    }
    public static JavaInputDStream<ConsumerRecord<String, String>> getKafkaStream (JavaStreamingContext jssc
            , List<Tuple2<String,String>> kafkaParams , KafkaOffsetPersist kafkaOffsetPersist) {
        Map<String, Object> kps = new HashMap<>();
        for (int i = 0; i < kafkaParams.size(); i++) {
            Tuple2<String, String> tp2 = kafkaParams.get(i);
            kps.put(tp2._1(),tp2._2());
        }
        return getKafkaStream(jssc,kps,kafkaOffsetPersist);
    }

    public static JavaInputDStream<ConsumerRecord<String, String>> getDirectStream (JavaStreamingContext jssc
            ,Map<String, Object> kafkaParams,KafkaOffsetPersist kafkaOffsetPersist){
        String[] topicNames = kafkaOffsetPersist.getTopic().split(",");
        Collection<String> topics = Arrays.asList(topicNames);
        JavaInputDStream<ConsumerRecord<String, String>> directStream = null;
        directStream =
                KafkaUtils.createDirectStream(
                        jssc,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams)
                );
        return directStream;
    }

    public static JavaInputDStream<ConsumerRecord<String, String>> getKafkaStream (JavaStreamingContext jssc
            ,Map<String, Object> kafkaParams,KafkaOffsetPersist kafkaOffsetPersist){
        String[] topicNames = kafkaOffsetPersist.getTopic().split(",");
        Collection<String> topics = Arrays.asList(topicNames);

        List<TopicGroupPartitionOffset> groupTopicPartitionOffsets = kafkaOffsetPersist.getOffset();
        Map<TopicPartition, Long> fromOffsets = new HashMap<>();
        JavaInputDStream<ConsumerRecord<String, String>> directStream = null;
        String kafkaReset = kafkaParams.get("auto.offset.reset").toString();
        if(groupTopicPartitionOffsets.size() > 0 && kafkaReset.equals("latest"))
        {
            String brokerList = kafkaParams.get("bootstrap.servers").toString();
            Map<TopicAndPartition, Long> earliestOffsetMap = getEarliestOffset(brokerList, Arrays.asList(topicNames), "leaderLookup_groupId_" + new Date().getTime());
            Map<TopicAndPartition, Long> lastOffsetMap = getLastOffset(brokerList, Arrays.asList(topicNames), "leaderLookup_groupId_" + new Date().getTime());

            if(groupTopicPartitionOffsets.size() != earliestOffsetMap.keySet().size())
            {
                throw new RuntimeException("kafka partition quantity change,please reset partition and offsize!");
            }
            String formateTime = DateUtil.getFormateTime(DateUtil.FORMAT_YYYY_MM_DDHHMMSS);
            for (int i = 0; i < groupTopicPartitionOffsets.size(); i++) {
                TopicGroupPartitionOffset gtpo = groupTopicPartitionOffsets.get(i);
                Long earliestOffset = 0L;
                Boolean findTopic = false;
                for (Map.Entry<TopicAndPartition, Long> entry : earliestOffsetMap.entrySet()) {
                    String topic = entry.getKey().topic();
                    int partition = entry.getKey().partition();
                    if(topic.equals(gtpo.getTopic()) && gtpo.getPartition().intValue() == partition)
                    {
                        earliestOffset = entry.getValue();
                        findTopic=true;
                        break;
                    }
                }
                if(!findTopic)
                {
                    throw new RuntimeException("saved kafka topic  \""+gtpo.getTopic()+"\" not match input topic");
                }
                Long lastOffset = 0L;
                for (Map.Entry<TopicAndPartition, Long> entry : lastOffsetMap.entrySet()) {
                    String topic = entry.getKey().topic();
                    int partition = entry.getKey().partition();
                    if(topic.equals(gtpo.getTopic()) && gtpo.getPartition().intValue() == partition)
                    {
                        lastOffset = entry.getValue();
                        break;
                    }
                }

                if(gtpo.getLastestOffset().longValue()>=earliestOffset.longValue() && gtpo.getLastestOffset().longValue()< lastOffset.longValue())
                {

                }
                else
                {
                    System.out.println(formateTime + " repair topic:"+gtpo.getTopic() + " partition:"+ gtpo.getPartition() +" offset: " + gtpo.getLastestOffset() +" -> "+ lastOffset);
                    gtpo.setEarliestOffset(earliestOffset);
                    gtpo.setLastestOffset(lastOffset);
                }

////                if (gtpo.getLastestOffset().longValue() > lastOffset.longValue()) {
//                if (gtpo.getLastestOffset().longValue() > lastOffset.longValue()
//                        || gtpo.getLastestOffset().longValue() < earliestOffset.longValue()) {
//                    System.out.println("repair topic:"+gtpo.getTopic() + " partition:"+ gtpo.getPartition() +" offset: " + gtpo.getLastestOffset() +" -> "+ lastOffset);
//                    gtpo.setEarliestOffset(earliestOffset);
//                    gtpo.setLastestOffset(lastOffset);
//                }
                fromOffsets.put(new TopicPartition(gtpo.getTopic(), gtpo.getPartition()), gtpo.getLastestOffset());
            }
            for (Map.Entry<TopicPartition, Long> entry : fromOffsets.entrySet()) {
                System.out.println(formateTime + " createDirectStream,topic="+entry.getKey().topic() + ",partition="+entry.getKey().partition()+",offset=" + entry.getValue());
            }
            directStream = KafkaUtils.createDirectStream(
                    jssc,
                    LocationStrategies.PreferConsistent(),
                    ConsumerStrategies.<String, String>Assign(fromOffsets.keySet(), kafkaParams, fromOffsets)
            );
        }
        else
        {
            directStream =
                    KafkaUtils.createDirectStream(
                            jssc,
                            LocationStrategies.PreferConsistent(),
                            ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams)
                    );
        }
        return directStream;
    }

    public static JavaInputDStream<ConsumerRecord<String, String>> getKafkaStream (JavaStreamingContext jssc
            ,String topic
            ,String appName
            ,String groupId
            ,Map<String, Object> kafkaParams){

        Collection<String> topics = Arrays.asList(topic);
//        Map<String, Object> kafkaParams = processKafka.initKafkaParam(topic,groupId);
//        Map<String, Object> kafkaParams = processKafka.getKafkaParams();
        KafkaOffsetPersist kafkaOffsetPersist = new KafkaOffsetPersistForRedis(appName, topic, groupId);
        List<TopicGroupPartitionOffset> groupTopicPartitionOffsets = kafkaOffsetPersist.getOffset();
        Map<TopicPartition, Long> fromOffsets = new HashMap<>();
        JavaInputDStream<ConsumerRecord<String, String>> directStream = null;
        System.out.println("groupTopicPartitionOffsets:"+groupTopicPartitionOffsets.size());
        if(groupTopicPartitionOffsets.size() > 0)
        {
            for (int i = 0; i < groupTopicPartitionOffsets.size(); i++) {
                TopicGroupPartitionOffset gtpo = groupTopicPartitionOffsets.get(i);
                fromOffsets.put(new TopicPartition(gtpo.getTopic(), gtpo.getPartition()), gtpo.getLastestOffset());
            }
            directStream = KafkaUtils.createDirectStream(
                    jssc,
                    LocationStrategies.PreferConsistent(),
                    ConsumerStrategies.<String, String>Assign(fromOffsets.keySet(), kafkaParams, fromOffsets)
            );
        }
        else
        {
            directStream =
                    KafkaUtils.createDirectStream(
                            jssc,
                            LocationStrategies.PreferConsistent(),
                            ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams)
                    );
        }
        return directStream;
    }

    private static String[] getBorkerUrlFromBrokerList(String brokerlist) {
        String[] brokers = brokerlist.split(",");
        for (int i = 0; i < brokers.length; i++) {
            brokers[i] = brokers[i].split(":")[0];
        }
        return brokers;
    }

    private static Map<String, Integer> getPortFromBrokerList(String brokerlist) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        String[] brokers = brokerlist.split(",");
        for (String item : brokers) {
            String[] itemArr = item.split(":");
            if (itemArr.length > 1) {
                map.put(itemArr[0], Integer.parseInt(itemArr[1]));
            }
        }
        return map;
    }
    final static int TIMEOUT = 10000;
    final static int BUFFERSIZE = 64 * 1024;

    public static Map<TopicAndPartition, Long> getLastOffset(String brokerList, List<String> topics,
                                                             String groupId) {
        Map<TopicAndPartition, Long> topicAndPartitionLongMap = new HashMap<>();
        Map<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerMap = findLeader(brokerList, topics);
        for (Map.Entry<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerEntry : topicAndPartitionBrokerMap
                .entrySet()) {
            // get leader broker
            BrokerEndPoint leaderBroker = topicAndPartitionBrokerEntry.getValue();
            SimpleConsumer simpleConsumer = new SimpleConsumer(leaderBroker.host(), leaderBroker.port(),
                    TIMEOUT, BUFFERSIZE, groupId);
            long readOffset = getTopicAndPartitionLastOffset(simpleConsumer,
                    topicAndPartitionBrokerEntry.getKey(), groupId);
            topicAndPartitionLongMap.put(topicAndPartitionBrokerEntry.getKey(), readOffset);
        }
        return topicAndPartitionLongMap;
    }

    public static Map<TopicAndPartition, Long> getEarliestOffset(String brokerList, List<String> topics,
                                                                 String groupId) {
        Map<TopicAndPartition, Long> topicAndPartitionLongMap =  new HashMap<>();
        Map<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerMap = findLeader(brokerList, topics);
        for (Map.Entry<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerEntry : topicAndPartitionBrokerMap
                .entrySet()) {
            BrokerEndPoint leaderBroker = topicAndPartitionBrokerEntry.getValue();
            SimpleConsumer simpleConsumer = new SimpleConsumer(leaderBroker.host(), leaderBroker.port(),
                    TIMEOUT, BUFFERSIZE, groupId);
            long readOffset = getTopicAndPartitionEarliestOffset(simpleConsumer,
                    topicAndPartitionBrokerEntry.getKey(), groupId);
            topicAndPartitionLongMap.put(topicAndPartitionBrokerEntry.getKey(), readOffset);

        }
        return topicAndPartitionLongMap;
    }

    public static long getTopicAndPartitionLastOffset(SimpleConsumer consumer,
                                                      TopicAndPartition topicAndPartition, String clientName) {
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo =
                new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(
                kafka.api.OffsetRequest.LatestTime(), 1));
        OffsetRequest request = new OffsetRequest(
                requestInfo, kafka.api.OffsetRequest.CurrentVersion(),
                clientName);
        OffsetResponse response = consumer.getOffsetsBefore(request);
        if (response.hasError()) {
            System.out
                    .println("Error fetching data Offset Data the Broker. Reason: "
                            + response.errorCode(topicAndPartition.topic(), topicAndPartition.partition()));
            return 0;
        }
        long[] offsets = response.offsets(topicAndPartition.topic(), topicAndPartition.partition());
        return offsets[0];
    }

    public static long getTopicAndPartitionEarliestOffset(SimpleConsumer consumer,
                                                          TopicAndPartition topicAndPartition, String clientName) {
        Map<TopicAndPartition, PartitionOffsetRequestInfo> requestInfo =
                new HashMap<TopicAndPartition, PartitionOffsetRequestInfo>();
        requestInfo.put(topicAndPartition, new PartitionOffsetRequestInfo(
                kafka.api.OffsetRequest.EarliestTime(), 1));
        OffsetRequest request = new OffsetRequest(
                requestInfo, kafka.api.OffsetRequest.CurrentVersion(),
                clientName);
        OffsetResponse response = consumer.getOffsetsBefore(request);
        if (response.hasError()) {
            System.out
                    .println("Error fetching data Offset Data the Broker. Reason: "
                            + response.errorCode(topicAndPartition.topic(), topicAndPartition.partition()));
            return 0;
        }
        long[] offsets = response.offsets(topicAndPartition.topic(), topicAndPartition.partition());
        return offsets[0];
    }

    public static Map<TopicAndPartition, BrokerEndPoint> findLeader(String brokerList, List<String> topics) {
        String[] brokerUrlArray = getBorkerUrlFromBrokerList(brokerList);
        Map<String, Integer> brokerPortMap = getPortFromBrokerList(brokerList);
        Map<TopicAndPartition, BrokerEndPoint> topicAndPartitionBrokerMap = new HashMap<>();
        for (String broker : brokerUrlArray) {
            SimpleConsumer consumer = null;
            try {
                consumer = new SimpleConsumer(broker, brokerPortMap.get(broker), TIMEOUT, BUFFERSIZE,
                        "leaderLookup" + new Date().getTime());
                TopicMetadataRequest req = new TopicMetadataRequest(topics);
                TopicMetadataResponse resp = consumer.send(req);
                List<TopicMetadata> metaData = resp.topicsMetadata();
                for (TopicMetadata item : metaData) {
                    for (PartitionMetadata part : item.partitionsMetadata()) {
                        TopicAndPartition topicAndPartition =
                                new TopicAndPartition(item.topic(), part.partitionId());
                        topicAndPartitionBrokerMap.put(topicAndPartition, part.leader());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (consumer != null)
                    consumer.close();
            }
        }
        return topicAndPartitionBrokerMap;
    }

    @Deprecated
    public static KafkaProducer<String, String> createProducer() {
        String kafkaBroker = ConfigUtil.getConfig().getString("kafkaparam.bootstrap.servers");
        Properties properties = new Properties();
        properties.put("bootstrap.servers", kafkaBroker);// kafka地址
        properties.put("acks", "-1");
//        properties.put("linger.ms", "50");
        properties.put("value.serializer", StringSerializer.class);
        properties.put("key.serializer", StringSerializer.class);
        return new KafkaProducer<String, String>((properties));
//        return producer;
    }

    @Deprecated
    public static void sendMessageNoClose(String topicName, String strMessage) {
        KafkaProducer<String, String> producer = createProducer();
        producer.send(new ProducerRecord<String, String>(topicName, strMessage));
    }

    @Deprecated
    public static void sendMessage(String topicName, String strMessage) {
        KafkaProducer<String, String> producer = createProducer();
        producer.send(new ProducerRecord<String, String>(topicName, strMessage));
        producer.close();
    }

    @Deprecated
    public static void sendMessageNoClose(String topicName, String... strMessages) {
        KafkaProducer<String, String> producer = createProducer();
        for (String strMessage : strMessages) {
            producer.send(new ProducerRecord<String, String>(topicName, strMessage));
        }
    }

    @Deprecated
    public static void sendMessage(String topicName, String... strMessages) {
        KafkaProducer<String, String> producer = createProducer();
        for (String strMessage : strMessages) {
            producer.send(new ProducerRecord<String, String>(topicName, strMessage));
        }
        producer.close();
    }

    @Deprecated
    public static void sendMessage(String topicName, List<String[]> strMessages,Boolean sendKey) {
        KafkaProducer<String, String> producer = createProducer();
        for (String[] strMessage : strMessages) {
            if(sendKey)
            {
                producer.send(new ProducerRecord<String, String>(topicName, strMessage[0],strMessage[1]));
            }
            else
            {
                producer.send(new ProducerRecord<String, String>(topicName, strMessage[1]));
            }
        }
        producer.close();
    }

    @Deprecated
    public static void sendMessage(String topicName, List<Map<Object, Object>> mapMessages) {
        KafkaProducer<String, String> producer = createProducer();
        for (Map<Object, Object> mapMessage : mapMessages) {
            String array = mapMessage.toString();
            producer.send(new ProducerRecord<String, String>(topicName, array));
        }
        producer.close();
    }

    @Deprecated
    public static void sendMessage(String topicName, Map<Object, Object> mapMessage) {
        KafkaProducer<String, String> producer = createProducer();
        String array = mapMessage.toString();
        producer.send(new ProducerRecord<String, String>(topicName, array));
        producer.close();
    }

}
